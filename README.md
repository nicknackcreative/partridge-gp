# Custom Wordpress Installation

This installation uses two custom installations: [Bedrock](https://roots.io/bedrock/) and a [custom Divi-child theme](web/app/themes/Divi-child/README.md).

## Requirements

* PHP >= 7.1
* Composer - [Install](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-osx)
* NodeJS & NPM - [Install](https://nodejs.org/en/)

## Quick Start

1. Add a file called `auth.json` to the root folder, containing the following (credentials to be provided):
```
{
    "http-basic": {
        "composer.wakeupdreamer.cloud": {
            "username": "",
            "password": ""
        }
    }
}
```
2. Run `composer install` from the root folder
3. Run `npm install` from the Divi-child theme folder
4. Create a new database using mysql. Import the database (there is an SQL dump in the sql folder)
5. Create a `.env` file in the root folder and copy in the contents of `.env.example`. 
6. Update environment variables in the `.env` file:
  * Database variables
    * `DB_NAME` - Database name
    * `DB_USER` - Database user
    * `DB_PASSWORD` - Database password
    * `DB_HOST` - Database host
    * Optionally, you can define `DATABASE_URL` for using a DSN instead of using the variables above (e.g. `mysql://user:password@127.0.0.1:3306/db_name`)
  * `WP_ENV` - Set to environment (`development`, `staging`, `production`)
  * `WP_HOME` - Full URL to WordPress home (https://example.com)
  * `WP_SITEURL` - Full URL to WordPress including subdirectory (https://example.com/wp)
  * `AUTH_KEY`, `SECURE_AUTH_KEY`, `LOGGED_IN_KEY`, `NONCE_KEY`, `AUTH_SALT`, `SECURE_AUTH_SALT`, `LOGGED_IN_SALT`, `NONCE_SALT`
    * Generate with [wp-cli-dotenv-command](https://github.com/aaemnnosttv/wp-cli-dotenv-command)
    * Generate with [Roots' WordPress salts generator](https://roots.io/salts.html)
7. Access WordPress admin at `https://example.com/wp/wp-admin/`


---

## :warning: Important notes on customising Divi
* Please read [here](web/app/themes/Divi-child/README.md) about the process for updating Divi


## :warning: Adding Plugins
* Please use composer to add all plugins. If a premium plugin is required, contact samuele@nicknack.com.au