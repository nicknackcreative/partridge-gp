<?php
namespace DiviChild\Functions\Custom;
/*
|--------------------------------------------------------------------------
| Add Your Custom Functions Here.
|--------------------------------------------------------------------------
|
| This file is included at functions.php.
| You may add any custom functionality below.
| If you are using Wordpress hooks, be sure to use the __NAMESPACE__ constant
| at the time of hooking.
| 
| --------------------------------------------------------------------------
| For Example:
| --------------------------------------------------------------------------
| 
|   add_action( 'init', __NAMESPACE__ . '\\my_custom_function' );
| 
|   function my_custom_function(){
|     echo 'This is a custom function';
|   }
|
*/

