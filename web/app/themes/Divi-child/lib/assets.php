<?php
namespace DiviChild\Functions\Assets;

/**
 * Enqueue theme assets
 */
function assets()
{
    // Parent Styles
    wp_enqueue_style('parent-style', get_template_directory_uri() . '/style.css');

    // Child Styles & Scripts (using Laravel Mix)
    // wp_enqueue_style('child-style-custom', get_stylesheet_directory_uri() . '/dist/styles/app.css');
    // wp_enqueue_script('child-js',  get_stylesheet_directory_uri() . '/dist/scripts/app.js', ['jquery'], null, true);

    // Child Styles & Scripts (not using Laravel Mix)
    wp_enqueue_style('child-style-override', get_stylesheet_directory_uri() . '/style.css');
    wp_enqueue_script('child-js-override',  get_stylesheet_directory_uri() . '/scripts.js', ['jquery'], null, true);

    /**
     * If not using Laravel Mix to compile assets (or if pulling styles and scripts from another source - eg. Google Fonts where an @import hasn't been used), 
     * enqueue custom scripts and styles here
     */



}

add_action('wp_enqueue_scripts', __NAMESPACE__ . '\\assets', 100);

