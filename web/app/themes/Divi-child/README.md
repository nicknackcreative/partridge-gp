# Divi Child

Divi Child is a modern, customisable child theme for Divi.

## Features

- [Laravel Mix](https://laravel.com/docs/master/mix) (Mix) for asset compilation
- Simple CSS customisation for those who don't want to use the Mix workflow

## Requirements

- In order to use Mix, you must have Node & NPM installed. However, this theme can be used without.

## :warning: Important notes on customising Divi
* Where possible, customise Divi in the global site settings, under `Appearance > Customise`.
* Page level customisations should be done from within the page, avoiding custom CSS or JS inside the page editor.
* **Wherever custom CSS, Javascript or code is required, DO NOT use Divi's Custom CSS module or the Integrations tab** (both under Divi > Theme Options) - use the [child theme tools](#using-laravel-mix---recommended) instead, in order to better version control the code, and to avoid absolute URLs where possible.

---

# Using Laravel Mix - Recommended

## Installation

1. Run `npm install` from within the Divi-child folder.

## Customising CSS

1. Add custom styles to `assets/styles/_global.scss`

  - You can add new sass stylesheets inside of the assets/styles folder, but be sure to import them inside of `assets/styles/app.scss`

2. Run `npm run dev` from the Divi-child folder to build assets for development or `npm run production` for production.

See [Laravel Mix](https://laravel.com/docs/master/mix) for complete documentation.

## Customising Javascript

> By default, this theme is set up to use DOM based routing, based on <http://goo.gl/EUTi53> by Paul Irish.

1. Add custom javascript inside of `assets/scripts/app.js`

- For site-wide javascript, add your code inside of the `common` object.
- For page-specific javascript, create a new object (see below). The javascript will only be fired on body classes that match. If a body class contains a dash, replace the dash with an underscore when adding it to the object.

```
'my_new_page': {
	init: function () {
	// Custom Code Here

	},
	finalize: function () {
	// Custom Code Here

	},
},
```

## Adding third party dependencies

1. Install via npm
2. Import at the top of `assets/scripts/app.js`

```
// For example, to import the Sweet Alert 2 Library
import Swal from 'sweetalert2';
import 'sweetalert2/src/sweetalert2.scss';
```

3. Run `npm run dev` or `npm run production`

> Note: If a vendor doesn't provide their code via npm, you may enqueue them in `lib/assets.php`

---


# No Laravel Mix - Not Recommended

## Installation

1. No installation required.

## Customising CSS

1. Add custom styles to `style.css`

## Customising Javascript

1. Add custom javascript to `scripts.js`

## Adding third party dependencies
1. Enqueue scripts and styles inside of `lib/assets.php`

---

# Adding custom PHP functionality

> Note, to override parent theme templates, use WordPress' standard template override process (eg. copy template from parent theme into child theme and then customise)

1. Add any custom, non-template PHP functionality to `lib/custom.php` and it will be automatically called.

- Be sure to use the `__NAMESPACE__` constant when using WordPress actions and filters (see `lib/custom.php` for more information).
- You may also add custom php files inside of lib, as long as they are then included inside of the `$divi_includes` array, in `functions.php`

## Contributors

- [Wake Up, Dreamer](https://wakeupdreamer.com.au/)
