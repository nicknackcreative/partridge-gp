/*
|--------------------------------------------------------------------------
| Custom Javascript (Not Recommended)
|--------------------------------------------------------------------------
|
| It is recommended to add scripts to assets/scripts/app.js and then run
| "npm run dev" or "npm run production" from your terminal,(assuming you
| have run npm install from the Divi-child folder). 
| 
| Adding scripts below is more of a 'hack', but it does work if necessary.
|
*/


(function ($) {
	$('.et-l--header .header-section .right-wrapper .et_pb_toggle').click( function(e) {
		if($('body').hasClass('menu-open')) {
			$('body').removeClass('menu-open');
		} else {		
			$('body').addClass('menu-open');
		}
	});
	
	$('.et-l--header .header-menu-section .top-row .right-wrapper .et_pb_toggle').click( function(e) {
		if($('body').hasClass('menu-open')) {
			$('body').removeClass('menu-open');
		} else {		
			//$('body').addClass('menu-open');
		}
	});
})(jQuery);