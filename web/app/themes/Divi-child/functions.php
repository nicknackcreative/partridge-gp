<?php

/*
|--------------------------------------------------------------------------
| Divi Child Theme Functions
|--------------------------------------------------------------------------
|
| To add custom functions to this child theme, add them to lib/custom.php.
| Alternatively, add a new file in the lib folder and add it to the 
| $divi_includes array below.
| 
| Please note that missing files will produce a fatal error.
|
*/

$divi_includes = [
  'lib/assets.php', // Assets setup - enqueue scripts and stylesheets here.
  'lib/extras.php', // Miscellaneous Extras
  'lib/custom.php', // Custom Functions - add any custom functionality here.
];

foreach ($divi_includes as $file) {
    if (!$filepath = locate_template($file)) {
        trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
    }
    require_once $filepath;
}
unset($file, $filepath);
